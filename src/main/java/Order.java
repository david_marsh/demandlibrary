import java.math.BigDecimal;

enum OrderType {
    BUY,
    SELL
}

public class Order {
    private long orderId;
    private long userId;
    private float quantity;
    private BigDecimal price;
    private OrderType type;

    public Order(long orderId, long userId, float quantity, BigDecimal price, OrderType type) {
        this.orderId = orderId;
        this.userId = userId;
        this.quantity = quantity;
        this.price = price;
        this.type = type;
    }

    public OrderKey key() {
        return new OrderKey(price, type);
    }

    public long getId() {
        return orderId;
    }

    public float getQuantity() {
        return quantity;
    }

    public OrderType getType() {
        return type;
    }

    static class OrderKey {
        private OrderType type;
        private BigDecimal price;

        OrderKey(BigDecimal price, OrderType type) {
            this.type = type;
            this.price = price;
        }

        public BigDecimal getPrice() {
            return price;
        }

        public OrderType getType() {
            return type;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            OrderKey orderKey = (OrderKey) o;

            if (type != orderKey.type) return false;
            return price != null ? price.equals(orderKey.price) : orderKey.price == null;
        }

        @Override
        public int hashCode() {
            int result = type != null ? type.hashCode() : 0;
            result = 31 * result + (price != null ? price.hashCode() : 0);
            return result;
        }
    }
}
