import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class OrderService {

    private LiveOrderStore store;
    private Map<Long, Order.OrderKey> orderLookup;
    private long orderCount = 1;

    public OrderService(LiveOrderStore store) {
        this.store = store;
        this.orderLookup = new HashMap<>();
    }

    public long register(long userId, float quantity, BigDecimal price, OrderType type) {
        long orderId = 0;

        Order order = new Order(orderCount++, userId, quantity, price, type);
        Order.OrderKey orderKey = order.key();
        orderId = order.getId();

        orderLookup.put(orderId, orderKey);
        List<Order> orders = store.computeIfAbsent(orderKey, k -> new ArrayList<Order>());
        orders.add(order);

        return orderId;
    }

    public void cancel(long orderId) {
        Order.OrderKey key = orderLookup.get(orderId);
        List<Order> orders = store.get(key);
        List<Order> updatedOrders = orders.stream().filter(x -> x.getId()!=orderId).collect(Collectors.toList());
        if(updatedOrders.isEmpty())
            store.remove(key);
        else
            store.replace(key, updatedOrders);
    }

    public Order get(long orderId) {
        Order.OrderKey key = orderLookup.get(orderId);
        List<Order> orders = store.get(key);
        if(orders==null)
            return null;

        Optional<Order> order = orders.stream().filter(x -> orderId == x.getId()).findFirst();
        return order.get();
    }

    public List<SummaryOrder> getBoardSummary(OrderType type) {
        List<SummaryOrder> summary = store.keySet().stream().map(x -> {
            List<Order> orders = store.get(x);
            double total = orders.stream().filter(o -> o.getType()==type).mapToDouble(o -> o.getQuantity()).sum();
            SummaryOrder so = new SummaryOrder(x.getType(), x.getPrice(), total);
            return so;
        }).collect(Collectors.toList());

        sortSummary(type, summary);

        return summary;
    }

    private void sortSummary(OrderType type, List<SummaryOrder> summary) {
        int order = (type==OrderType.SELL)? 1 : -1;
        summary.sort((a,b) -> a.getPrice().compareTo(b.getPrice())*order);
    }
}
