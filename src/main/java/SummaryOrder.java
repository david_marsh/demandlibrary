import java.math.BigDecimal;
import java.text.MessageFormat;

public class SummaryOrder {
    private OrderType type;
    private BigDecimal price;
    private double quantity;

    public SummaryOrder(OrderType type, BigDecimal price, double quantity) {
        this.type = type;
        this.quantity = quantity;
        this.price = price;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public double getQuantity() {
        return quantity;
    }

    public String toString() {
        String msg = MessageFormat.format("{0} kg for £{1}", quantity, price);
        return msg;
    }
}
