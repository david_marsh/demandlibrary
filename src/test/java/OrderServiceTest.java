import org.junit.Test;
import org.mockito.Matchers;

import static java.lang.System.out;
import static junit.framework.TestCase.assertEquals;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class OrderServiceTest {

    @Test
    public void registerRegistersAnOrder() {
        LiveOrderStore store = mock(LiveOrderStore.class);
        OrderService orderService =  new OrderService(store);

        when(store.computeIfAbsent(any(Order.OrderKey.class), any())).thenReturn(new ArrayList<Order>());

        // Test can register
        long orderId = orderService.register(1, 3.5f, BigDecimal.valueOf(303), OrderType.BUY);

        assertThat((int)orderId, greaterThan(0));
        verify(store, times(1)).computeIfAbsent(eq(new Order.OrderKey(BigDecimal.valueOf(303), OrderType.BUY)), any());
    }

    @Test
    public void cancelAnOrder() {
        LiveOrderStore store = new LiveOrderStore();
        OrderService orderService =  new OrderService(store);

        long orderId = orderService.register(1, 3.5f, BigDecimal.valueOf(303), OrderType.BUY);

        // Test can cancel
        orderService.cancel(1);

        assertNull(orderService.get(1));
    }

/**
    3) Get summary information of live orders (see explanation below)
    Imagine we have received the following orders:
            -	a) SELL: 3.5 kg for £306 [user1]
            -	b) SELL: 1.2 kg for £310 [user2]
            -	c) SELL: 1.5 kg for £307 [user3]
            -	d) SELL: 2.0 kg for £306 [user4]

    Our ‘Live Order Board’ should provide us the following summary information:
            -	5.5 kg for £306 // order a + order d
            -	1.5 kg for £307 // order c
            -	1.2 kg for £310 // order b
*/
    @Test
    public void getSummarySellOrders() {
        LiveOrderStore store = new LiveOrderStore();
        OrderService orderService =  new OrderService(store);

        long orderId2 = orderService.register(2, 1.2f, BigDecimal.valueOf(310), OrderType.SELL);
        long orderId1 = orderService.register(1, 3.5f, BigDecimal.valueOf(306), OrderType.SELL);
        long orderId3 = orderService.register(3, 1.5f, BigDecimal.valueOf(307), OrderType.SELL);
        long orderId4 = orderService.register(4, 2.0f, BigDecimal.valueOf(306), OrderType.SELL);

        List<SummaryOrder> summary = orderService.getBoardSummary(OrderType.SELL);

        assertEquals(summary.get(0).getQuantity(), 5.5, 0.0001);
        assertEquals(summary.get(1).getQuantity(), 1.5, 0.0001);
        assertEquals(summary.get(2).getQuantity(), 1.2, 0.0001);

        summary.stream().forEach(System.out::println);
    }

    @Test
    public void getSummaryBuyOrders() {
        LiveOrderStore store = new LiveOrderStore();
        OrderService orderService =  new OrderService(store);

        long orderId1 = orderService.register(1, 3.5f, BigDecimal.valueOf(306), OrderType.BUY);
        long orderId2 = orderService.register(2, 1.2f, BigDecimal.valueOf(310), OrderType.BUY);
        long orderId3 = orderService.register(3, 1.5f, BigDecimal.valueOf(307), OrderType.BUY);
        long orderId4 = orderService.register(4, 2.0f, BigDecimal.valueOf(306), OrderType.BUY);

        List<SummaryOrder> summary = orderService.getBoardSummary(OrderType.BUY);

        assertEquals(summary.get(0).getQuantity(), 1.2, 0.0001);
        assertEquals(summary.get(1).getQuantity(), 1.5, 0.0001);
        assertEquals(summary.get(2).getQuantity(), 5.5, 0.0001);

        summary.stream().forEach(System.out::println);
    }
}
